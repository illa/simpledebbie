define([
    "jquery",
    "ace",

    "../lib/ace/theme-dreamweaver",
    "../lib/ace/theme-ambiance",
], function($, ace) {

    var editor,
        highlight,
        breakpoints,
        errors,
        removedBreakpoints,
        toggleBreakpoint;

    function setToggleBreakpoint(toggleBreakpoint_) {
        toggleBreakpoint = toggleBreakpoint_;
    }

    function init(editorMode) {
        editor = ace.edit("editor")

        editor.getSession().setMode(editorMode);
        editor.setFontSize(15);

        highlight = -1;
        breakpoints = [];
        removedBreakpoints = [];
        errors = [];
        
        editor.on("guttermousedown", function(e) { 
            var target = e.domEvent.target,
                index,
                row;
            if (! $(target).hasClass("ace_gutter-cell")) {
                return;
            }
            row = $(target).index() + editor.getFirstVisibleRow();

            if (toggleBreakpoint == undefined) {
                return;
            }

            index = breakpoints.indexOf(row);
            if (index >= 0) {
                editor.getSession().removeGutterDecoration(row, "ace_warning");
                breakpoints.splice(index, 1);
            } else {
                editor.getSession().addGutterDecoration(row, "ace_warning");
                breakpoints.push(row);
            }
            toggleBreakpoint(row);
        });
    }

    function clearErrorsAndHighlight() {
        if (highlight > -1) {
            editor.getSession().removeGutterDecoration(highlight, "highlight-row");
        }
        errors.forEach(function (e) {
            editor.getSession().removeGutterDecoration(e, "ace_error");         
        });
        removedBreakpoints.forEach(function (b) {
            editor.getSession().addGutterDecoration(b, "ace_warning");          
        });
        removedBreakpoints = [];
        errors = [];
        highlight = -1;
    }

    function setErrors(lines) {
        lines.forEach(function (line) {
            var index = breakpoints.indexOf(line-1);
            if (index >= 0) {
                editor.getSession().removeGutterDecoration(line-1, "ace_warning");
                removedBreakpoints.push(line-1);                        
            }
            errors.push(line-1);
            editor.getSession().addGutterDecoration(line-1, "ace_error");
        });
    }

    function getText() {
        return editor.getSession().getValue();
    }

    function setText(text) {
        editor.getSession().setValue(text);
    }

    function setTheme(theme) {
        if (editor == undefined) return;
        if (theme == "dark") {
            editor.setTheme("ace/theme/ambiance");            
        } else {
            editor.setTheme("ace/theme/dreamweaver");            
        }
    }

    function setOnTextChangeListener(callback) {
        editor.getSession().on('change', callback);
    }

    function resize() {
        editor.resize(true);
    }

    function highlightLine(line) {
        editor.gotoLine(line);
        if (highlight > -1) {
            editor.getSession().removeGutterDecoration(highlight, 'highlight-row');         
        }
        editor.getSession().addGutterDecoration(line-1, 'highlight-row');
        highlight = line - 1;
    }

    return {
        init: init,
        setToggleBreakpoint: setToggleBreakpoint,
        setOnTextChangeListener: setOnTextChangeListener,
        clearErrorsAndHighlight: clearErrorsAndHighlight,
        setErrors: setErrors,
        getText: getText,
        setText: setText,
        setTheme: setTheme,
        resize: resize,
        highlightLine: highlightLine,
    }
});
