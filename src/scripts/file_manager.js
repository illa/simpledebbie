define([
    "new_file_view",
    "load_view",
    "save_as_view",
    "save_changes_view",
    "local_storage_tester",
], function(newFileView, loadView, saveAsView, saveChanges, storageTester) {
    
    function init() {
        if (storageTester.isAvaible() == true) {
            if (localStorage.getItem('debbie_programs') == null) {
                localStorage.debbie_programs = JSON.stringify({'...': { name: '...', list: false, text: ''}});
            }
            if (localStorage.getItem('debbie_current') == null) {
                localStorage.debbie_current = '...';
            }
            _loadCurrent();
        } else {
            _disableButtons();
        }
    }

    function _disableButtons() {
        $("#show-load-modal").removeClass("my-button");
        $("#show-load-modal").addClass("disabled-button");
        $("#save-current").removeClass("my-button");
        $("#save-current").addClass("disabled-button");
        $("#show-save-as-modal").removeClass("my-button");
        $("#show-save-as-modal").addClass("disabled-button");
        
        $("#show-load-modal").off();
        $("#save-current").off();
        $("#show-save-as-modal").off();
    }
        
    function _loadCurrent() {
        loadView.loadCurrent();
    }

    return {
        init: init,
    };
    
});
