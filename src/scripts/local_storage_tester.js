define( function () {
    
    function isAvaible() {
        try {
            localStorage.setItem('debbie_test', 'debbie_test');
            localStorage.removeItem('debbie_test');
            return true;
        } catch(e) {
            return false;
        }
    }

    return {
        isAvaible: isAvaible,
    };

});

