define([
    "jquery",

    "save_changes_view",
    "editor_manager",
], function ($, saveChangesView, editorManager) {

    $("#new-file").click(_newFile);

    function _clearEditor() {
        var current = '...';
        editorManager.setText('');
        $('#current-program').html(current);
        try {
            localStorage.debbie_current = current;
        } catch (e) {
        }
    }

    function _newFile() {
        saveChangesView.check(_clearEditor);
    }

});
