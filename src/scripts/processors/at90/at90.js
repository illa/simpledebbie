define([
    "processors/at90/at90_comp",
    "processors/at90/at90_proc",

    "../../../lib/ace/mode-assembly_atmel",
], function(compiler, processor) {

    function assemble(text) {
        return compiler.assemble(text);
    }

    function load(program) {
        processor.load(program);
    }

    /* return list of errors [ {line: number, type: 'text'}, ... ]*/
    function getErrors() {
        return compiler.getErrors();
    }

    /* return a line a command from given address came from */
    function whatLine(address) {
        return compiler.whatLine(address);
    }

    function reset() {
        processor.reset();
    }

    function next() {
        return processor.next();
    }

    /* return number */
    function getProgramCounter() {
        return processor.getProgramCounter();
    }

    /* return number */
    function getCycles() {
        return processor.getCycles();
    }

    /* return [ {name: 'name', value: number}, ... ] */
    function getFlags() {
        return processor.getFlags();
    }

    /* return [ {name: 'name', value: number}, ... ] */
    function getRegisters() {
        return processor.getRegisters();
    }

    /* return [ number, ...] */
    function getMemory() {
        return processor.getMemory();
    }

    /* return [ number, ...] */
    function getProgramMemory() {
        return processor.getProgramMemory();
    }

    /* return [ {name: 'name', start: start_address, end: end_address, delta: start_address - desired_index}, ...] */
    function getMemoryChoices() {
        return processor.getMemoryChoices();
    }

    function setProgramCounter(value) {
        processor.setProgramCounter(value);
    }

    function setCycles(value) {
        processor.setCycles(value);
    }

    function setFlag(name, value) {
        processor.setFlag(name, value);
    }

    function setRegister(name, value) {
        processor.setRegister(name, value);
    }

    function setMemory(index, value) {
        processor.setMemory(index, value);
    }

    function setProgramMemory(index, value) {
        processor.setProgramMemory(index, value);
    }

    function getProgramCounterIndex() {
        return processor.getProgramCounterIndex();
    }

    function getProgramMaxSize() {
        return processor.getProgramMaxSize();
    }

    return {
        getName: function() { return "AT90USB1287"; },
        getEditorMode: function() { return "ace/mode/assembly_atmel"; },
        getMemoryChoices: getMemoryChoices,
        whatLine: whatLine,
        getErrors: getErrors,
        assemble: assemble,
        reset: reset,
        load: load,
        next: next,
        getProgramCounter: getProgramCounter,
        getCycles: getCycles,
        getFlags: getFlags,
        getRegisters: getRegisters,
        getMemory: getMemory,
        getProgramMemory: getProgramMemory,
        setProgramCounter: setProgramCounter,
        setCycles: setCycles,
        setFlag: setFlag,
        setRegister: setRegister,
        setMemory: setMemory,
        setProgramMemory: setProgramMemory,
        getProgramCounterIndex: getProgramCounterIndex,
        getProgramMaxSize: getProgramMaxSize,
    };
});
