define(['processors/at90/expressions', 'processors/at90/at90_opcodes' ], function(expressionsParser, opcodeTranslator) {
    
    var program = [], //tablica bajtów
        errors = [],
        labels = {},
        defined = {},
        addressToLine = {}, //address -> line
        address = 0, //w bajtach
        addressE, //w bajtach
        addressD, //w bajtach
        seg = 'cseg',
        commands = {
            adc: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]}, 
            add: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]}, 
            adiw : {words: 1, args: [{values: [24, 26, 28, 30], type: 'register_pair'}, {min: 0, max: 63, type: 'constant'}]},
            and: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]}, 
            andi: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {min: 0, max: 255, type: 'constant'}]}, 
            asr: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            bclr: {words: 1, args: [{min: 0, max: 7, type: 'constant'}]},
            bld: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 7, type: 'constant'}]},
            brbc: {words: 1, args: [{min: 0, max: 7, type: 'constant'}, {min: -64, max: 63, type: 'relative_constant'}]},
            brbs: {words: 1, args: [{min: 0, max: 7, type: 'constant'}, {min: -64, max: 63, type: 'relative_constant'}]},
            break: {words: 1, args: []},
            brcc: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brcs: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            breq: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brge: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brhc: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brhs: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brid: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brie: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brlo: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brlt: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brmi: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brne: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brpl: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brsh: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brtc: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brts: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brtc: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            brts: {words: 1, args: [{min: -64, max: 63, type: 'relative_constant'}]},
            bset: {words: 1, args: [{min: 0, max: 7, type: 'constant'}]},
            bst: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 7, type: 'constant'}]},
            call: {words: 2, args: [{min: 0, max: 4194303, type: 'constant'}]},
            cbi: {words: 1, args: [{min: 0, max: 31, type: 'constant'}, {min: 0, max: 7, type: 'constant'}]},
            cbr: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {min: 0, max: 255, type: 'constant'}]},
            clc: {words: 1, args: []},
            clh: {words: 1, args: []},
            cli: {words: 1, args: []},
            cln: {words: 1, args: []},
            clr: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            cls: {words: 1, args: []},
            clt: {words: 1, args: []},
            clv: {words: 1, args: []},
            clz: {words: 1, args: []},
            com: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            cp: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            cpc: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            cpi: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {min: 0, max: 255, type: 'constant'}]},
            cpse: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            dec: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            des: {words: 1, args: [{min: 0, max: 15, type: 'constant'}]},
            eicall: {words: 1, args: []}, //?
            eijmp: {words: 1, args: []}, //?
            elpm:  {words: 1, args: []}, //?
            eor: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            fmul: {words: 1, args: [{min: 16, max: 23, type: 'register'}, {min: 16, max: 23, type: 'register'}]}, //?
            fmuls: {words: 1, args: [{min: 16, max: 23, type: 'register'}, {min: 16, max: 23, type: 'register'}]}, //?
            fmulsu: {words: 1, args: [{min: 16, max: 23, type: 'register'}, {min: 16, max: 23, type: 'register'}]}, //?
            icall: {words: 1, args: []}, //?
            ijmp: {words: 1, args: []}, //?
            in: {words: 1,  args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 63, type: 'constant'}]},
            inc: {words: 1,  args: [{min: 0, max: 31, type: 'register'}]},
            jmp: {words: 2, args: [{min: 0, max: 4194303, type: 'constant'}]},
            lac: {words: 1, args: [{values: ['z'], type: 'register_name'}, {min: 0, max: 31, type: 'register'}]},
            las: {words: 1, args: [{values: ['z'], type: 'register_name'}, {min: 0, max: 31, type: 'register'}]},
            lat: {words: 1, args: [{values: ['z'], type: 'register_name'}, {min: 0, max: 31, type: 'register'}]},
            ld: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {type: 'register_name', values: ['x','y','z','x+','y+','z+','-x','-y','-z']}]},          
            ldi: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {type: 'constant', min: 0, max: 255}]},
            lds: {words: 2, args: [{min: 0, max: 31, type: 'register'}, {type: 'constant', min: 0, max: 65535}]}, //?
            lpm: {words: 1, args: [{min: 0, max: 31, type: 'register', default: 0}, {type: 'register_name', values: ['z','z+'], default: 'z'}]},
            lsl: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            lsr: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            mov: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            movw : {words: 1, args: [{values: [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30], type: 'register_pair'}, {values: [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30], type: 'register_pair'}]},
            mul: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            muls: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            mulsu: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            neg: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            nop: {words: 1, args: []},
            or: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            ori: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {type: 'constant', min: 0, max: 255}]},
            out: {words: 1,  args: [{min: 0, max: 63, type: 'constant'}, {min: 0, max: 31, type: 'register'}]},
            pop: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            push: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            rcall: {words: 1, args: [{min: -2048, max: 2048 , type: 'relative_constant'}]},
            ret: {words: 1, args: []},
            reti: {words: 1, args: []},
            rjmp: {words: 1, args: [{min: -2048, max: 2048 , type: 'relative_constant'}]},
            rol: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            ror: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            sbc: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            sbci: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {min: 0, max: 255, type: 'constant'}]},
            sbi: {words: 1, args: [{min: 0, max: 31, type: 'constant'}, {min: 0, max: 7, type: 'constant'}]},
            sbic: {words: 1, args: [{min: 0, max: 31, type: 'constant'}, {min: 0, max: 7, type: 'constant'}]},
            sbis: {words: 1, args: [{min: 0, max: 31, type: 'constant'}, {min: 0, max: 7, type: 'constant'}]},
            sbiw: {words: 1, args: [{values: [24, 26, 28, 30], type: 'register_pair'}, {min: 0, max: 63, type: 'constant'}]},
            sbr: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {min: 0, max: 255, type: 'constant'}]},
            sbrc: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 7, type: 'constant'}]},
            sbrs: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 7, type: 'constant'}]},
            sec: {words: 1, args: []},
            seh: {words: 1, args: []},
            sei: {words: 1, args: []},
            sen: {words: 1, args: []},
            ser: {words: 1, args: [{min: 16, max: 31, type: 'register'}]},
            ses: {words: 1, args: []},
            set: {words: 1, args: []},
            sev: {words: 1, args: []},
            sez: {words: 1, args: []},
            sleep: {words: 1, args: []},
            spm: {words: 1, args: [{type: 'register_name', values: ['z+'], default: 'z'}] },
            st: {words: 1, args: [{type: 'register_name', values: ['x','y','z','x+','y+','z+','-x','-y','-z']}, {min: 0, max: 31, type: 'register'}]},
            sts: {words: 2, args: [{min: 0, max: 65535, type: 'constant'}, {min: 0, max: 31, type: 'register'}]},
            sub: {words: 1, args: [{min: 0, max: 31, type: 'register'}, {min: 0, max: 31, type: 'register'}]},
            subi: {words: 1, args: [{min: 16, max: 31, type: 'register'}, {min: 0, max: 255, type: 'constant'}]},
            swap: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            tst: {words: 1, args: [{min: 0, max: 31, type: 'register'}]},
            wdr: {words: 1, args: []},
            xch: {words: 1, args: [{type: 'register_name', values: ['z']}, {min: 0, max: 31, type: 'register'}]}            
        };
        
    function _parseArgument(text, arg) {
        var registerPattern = /^\s*r\s*(\d+)\s*$/,
            registerPairPattern = /^\s*r(\d+)\s*:\s*r(\d+)\s*$/,
            registerNamePattern = /^\s*([xyz])\s*$/,
            registerNamePlusPattern = /^\s*([xyz])\s*\+\s*$/,
            registerNameMinusPattern = /^\s*\-\s*([xyz])\s*$/,
            value;

        if (arg.type == 'constant' || arg.type == 'relative_constant') {
            return {ok: true, value: text};
        } else if (arg.type == 'register') {
            for (def in defined) {
                var string = '(^|\\W+)'+def+'($|\\W+)';
                var string2 = '$1'+defined[def]+'$2';
                var pattern = new RegExp(string,'g');
                text = text.replace(pattern, string2);
            }
            if(! registerPattern.test(text)) {
                return {ok: false, value: 0};
            }
            value = Number(registerPattern.exec(text)[1]);
            if ((arg.min <= value && arg.max >= value) || (arg.values != undefined && arg.values.indexOf(value) >= 0) ) {
                return {ok: true, value: value};
            }
        } else if (arg.type == 'register_pair') {
            for (def in defined) {
                var string = '(^|\\W+)'+def+'($|\\W+)';
                var string2 = '$1'+defined[def]+'$2';
                var pattern = new RegExp(string,'g');
                text = text.replace(pattern, string2);
            }
            if(! registerPairPattern.test(text)) {
                return {ok: false, value: 0};
            }

            value = registerPairPattern.exec(text);
            var val1 = Number(value[1]),
                val2 = Number(value[2]);
            if (val2 + 1 == val1 && 
                ((arg.min <= val2 && arg.max >= val2) || (arg.values != undefined && arg.values.indexOf(val2) >= 0)) ) {
                return {ok: true, value: val2};
            }
        } else if (arg.type == 'register_name' && registerNamePattern.test(text)) {
            value = registerNamePattern.exec(text)[1];
            if (arg.values != undefined && arg.values.indexOf(value) >= 0 ) {
                return {ok: true, value: {value: value, delta: 0}};
            }
        } else if (arg.type == 'register_name' && registerNamePlusPattern.test(text)) {
            value = registerNamePlusPattern.exec(text)[1];
            if (arg.values != undefined && arg.values.indexOf(value) >= 0 ) {
                return {ok: true, value: {value: value, delta: 1}};
            }
        } else if (arg.type == 'register_name' && registerNameMinusPattern.test(text)) {
            value = registerNameMinusPattern.exec(text)[1];
            if (arg.values != undefined && arg.values.indexOf(value) >= 0 ) {
                return {ok: true, value: {value: value, delta: -1}};
            }
        } 
        return {ok: false, value: 0};
    }
    
    function _parseDirective(text, i) {
        var def = /^\s*.def\s+(\w+)\s*=\s*(r\d+)\s*$/,
            equ = /^\s*.equ\s+(\w+)\s*=(.*)$/,
            set = /^\s*.set\s+(\w+)\s*=(.*)$/,
            db = /^\s*.db\s+(.*)/,
            dw = /^\s*.dw\s+(.*)/,
            dd = /^\s*.dd\s+(.*)/,
            dq = /^\s*.dq\s+(.*)/,
            org = /^\s*.org\s+(.*)/,
            cseg = /^\s*.cseg\s+$/,
            dseg = /^\s*.dseg\s+$/,
            undef = /^\s*.undef\s+(\w+)\s*$/,
            value;
        
        if (def.test(text)) {
            value = def.exec(text);
            //and check if register is valid 
            if (! (value[1] in labels)) {
                defined[value[1]] = value[2];
            } else {
                errors.push({line: i+1, type: 'label already defined'});              
            }
        } else if (undef.test(text)) { //troche bez sensu
            value = undef.exec(text);
            //and check if register is valid 
            if (value[1] in defined) {
                delete defined[value[1]];
            } else {
                errors.push({line: i+1, type: 'label not defined'});                
            } 
        } else if (equ.test(text)) {
            value = equ.exec(text);
            if (! (value[1] in labels || value[1] in defined)) {
                try {
                    labels[value[1]] = expressionsParser.parse(value[2]);       
                } catch (e) {
                    errors.push({line: i+1, type: 'constant expression error'});
                }               
            } else {
                errors.push({line: i+1, type: 'label already defined'});
            }
        } else if (db.test(text)) {
            value = db.exec(text)[1].split(',');
            value.forEach(function (v) {
                if (program[address] != undefined) {
                    errors.push({line: i+1, type: 'overlapping commands on address' + address});                                                    
                }
                program[address] = {data: v};
                address += 1;
            });
            if (address % 2 == 1) {
                program[address] = {data: '0'};
                address += 1;               
            }
        } else if (dw.test(text)) {
            value = dw.exec(text)[1].split(',');
            value.forEach(function (v) {
                if (program[address] != undefined) {
                    errors.push({line: i+1, type: 'overlapping commands on address' + address});                                                    
                }
                program[address] = { data: '(' + v + ') % 256'};
                program[address+1] = { data: '((' + v + ') >> 8 ) % 256'};
                address += 2;
            });
        } else if (dd.test(text)) {
            value = dd.exec(text)[1].split(',');
            value.forEach(function (v) {
                if (program[address] != undefined) {
                    errors.push({line: i+1, type: 'overlapping commands on address' + address});                                                    
                }
                program[address] = { data: '(' + v + ') % 256'};
                program[address+1] = { data: '((' + v + ') >> 8 ) % 256'};
                program[address+2] = { data: '((' + v + ') >> 16 ) % 256'};
                program[address+3] = { data: '((' + v + ') >> 24 ) % 256'};
                address += 4;
            });
        } else if (dq.test(text)) {
            value = dq.exec(text)[1].split(',');
            value.forEach(function (v) {
                if (program[address] != undefined) {
                    errors.push({line: i+1, type: 'overlapping commands on address' + address});                                                    
                }
                program[address] = { data: '(' + v + ') % 256'};
                program[address+1] = { data: '((' + v + ') >> 8 ) % 256'};
                program[address+2] = { data: '((' + v + ') >> 16 ) % 256'};
                program[address+3] = { data: '((' + v + ') >> 24 ) % 256'};
                program[address+4] = { data: '((' + v + ') >> 32 ) % 256'};
                program[address+5] = { data: '((' + v + ') >> 40 ) % 256'};
                program[address+6] = { data: '((' + v + ') >> 48 ) % 256'};
                program[address+7] = { data: '((' + v + ') >> 56 ) % 256'};
                address += 8;
            });
        } else if (org.test(text)) {
            value = org.exec(text);
            if (seg == 'cseg') {
                try {
                    address = 2 * expressionsParser.parse(value[1]);
                } catch (e) {
                    errors.push({line: i+1, type: 'constant expression error'});
                }               
            } else if (seg == 'dseg') {
                try {
                    addressD = expressionsParser.parse(value[1]);
                } catch (e) {
                    errors.push({line: i+1, type: 'constant expression error'});
                }       
            }       
        } else if (cseg.test(text)) {
            seg = 'cseg';
        } else if (dseg.test(text)) {
            seg = 'dseg';
        }
        
    }
        
    function _computeConstants(arg, text, address) {
        for (label in labels) {
            var string = '(^|\\W+)'+label+'($|\\W+)';
            var string2 = '$1'+labels[label]+'$2';
            var pattern = new RegExp(string,'g');
            text = text.replace(pattern, string2);
        }
        try {
            value = expressionsParser.parse(text);
        } catch (e) {
            return {ok: false, value: 0};
        }               
        if (arg == undefined) {
            return {ok: false, value: value};
        }
        if (arg.type == 'relative_constant') {
            value = value - address - 1;
        }
        if (arg.min <= value && arg.max >= value) {
            return {ok: true, value: value};
        }
        return {ok: false, value: value};
    }

    function _replaceLabels() {
                
        for (var i = 0, max = program.length; i < max; i += 1) {
            if (program[i] == undefined) {
                continue;
            }
            if (program[i].args != undefined) {
                for (var j = 0; j < program[i].args.length; j += 1) {
                    if (typeof program[i].args[j] == 'string') { 
                        var arg = commands[program[i].name].args[j];
                        var result = _computeConstants(arg, program[i].args[j], i/2);
                        if (result.ok) {
                            program[i].args[j] = result.value;
                        } else {
                            errors.push({line: addressToLine[i]+1, type: 'invalid argument ' + (j+1)});
                        }                       
                    }
                }
            }
            if (program[i].data != undefined) {
                program[i].data = _computeConstants(undefined, program[i].data).value;
            }
        }
    }   

    function _removeComment(text) {
        var index = text.indexOf(';');
        if (index >= 0) {
            text = text.substr(0, index);
        }
        return text;
    }

    function _parseCommand(command, line, args) {
        var arguments = [],
            name = command[0],
            i,
            a = 1;
        addressToLine[address] = line;
        if (! (name in commands)) {
            errors.push({line: line+1, type: 'unknown command'});
            return;                     
        }
        if (commands[name].args.length != args) {
            errors.push({line: line+1, type: 'invalid amount of arguments, expected: ' + commands[name].args.length});
            return;                      
        }
        
        for (i = 0 ; i < args; i += 1) {
            arg = _parseArgument(command[i+1], commands[name].args[i]);
            if (arg.ok == true) {
                arguments.push(arg.value);
            } else {
                errors.push({line: line+1, type: 'invalid argument ' + (i+1)});
            }
        }
        
        if (program[address] != undefined) {
            errors.push({line: line+1, type: 'overlapping commands on address' + address});
        }
        program[address] = {name: name, args: arguments};
        while (a < 2*commands[name].words) {
            if (program[address + a] != undefined) {
                errors.push({line: line+1, type: 'overlapping commands on address' + (address+a)});                                                    
            }
            program[address + a] = {};
            a += 1;
        }
        address += 2*commands[name].words;
    }

    function _parseLabels(text, line) {
        text.split(/[: \t]+/).forEach( function(label) {
            if (label in commands) {
                errors.push({line: line+1, type: 'invalid label\'s name'});
                return;
            }
            if (label in labels) {
                errors.push({line: line+1, type: 'label already defined'});
                return;
            }
            if (label != '') {
                labels[label] = address / 2; // wartosci etykiet sa w słowach a nie bajtach
            }
        });
    }

    function _parseLine(text, line) {
        var linePattern = /^((\s*\w+:\s*)*)?([^;]*)?/,
            commandPatternArg0 = /^\s*(\w+)\s*$/,
            commandPatternArg1 = /^\s*(\w+)\s+(\w+)\s*$/,
            commandPatternArg2 = /^\s*(\w+)\s+([^,]*),(.*)$/,
            directivePattern = /^\s*\.\w+.*$/,
            lineParts,
            labelsText,
            commandText,
            commandParts;

        if (linePattern.test(text) != true) {
            errors.push({line: line+1, type: 'syntax error'});
            return;
        }
        lineParts = linePattern.exec(text);
        labelsText = lineParts[1];
        commandText = lineParts[3];
        
        if (labelsText != undefined) {
            _parseLabels(labelsText);
        }
        if (commandText != undefined && !/^\s*$/.test(commandText)) {
            if (commandPatternArg0.test(commandText)) {
                commandParts = commandPatternArg0.exec(commandText);
                _parseCommand(commandParts.slice(1), line, 0);
            } else if (commandPatternArg1.test(commandText)) {
                commandParts = commandPatternArg1.exec(commandText);
                _parseCommand(commandParts.slice(1), line, 1);
            } else if (commandPatternArg2.test(commandText)) {
                commandParts = commandPatternArg2.exec(commandText);
                _parseCommand(commandParts.slice(1), line, 2);
            } else if (directivePattern.test(commandText)) {
                _parseDirective(commandText, line);
            } else {
                errors.push({line: line+1, type: 'syntax error'});
            }
        }
    }
    
    function _parseProgram(text) {
        var lines = text.toLowerCase().split('\n'),
            i = 0,
            max = lines.length;
        
        address = 0;
        program = new Array();
        labels = {};
        defined = {};
        addressToLine = {};
        
        for (i = 0; i < max; i += 1) {
            _removeComment(lines[i]);
            _parseLine(lines[i], i);
        }

        _replaceLabels();
    }
    
    function assemble(text) {
        var programBin, bytes, i = 0, j;
        errors = [];
        _parseProgram(text);
        if (errors.length > 0) {
            return [];
        }

        programBin = new Array(program.length);
        while (i < program.length) {
            if (program[i] == null || Object.keys(program[i]).length == 0) {
                programBin[i] = 255;
                i += 1;
                continue;
            }
            if (program[i].data != undefined) {
                programBin[i] = program[i].data;
                i += 1;
                continue;
            }
            bytes = opcodeTranslator.toOpcode(program[i].name, program[i].args[0], program[i].args[1]);
            for (j = 0; j < bytes.length; j += 1) {
                programBin[i+j] = bytes[j];
            }
            i += j;
        }
        return programBin;
    }
    
    return {
        whatLine: function(address) {
            return addressToLine[address*2]+1;
        },
        getErrors: function() {
            return errors;
        },
        assemble: assemble,
    };
});
