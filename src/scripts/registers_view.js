define([
    "jquery",
    "jquery-ui",
    "underscore",

    "edit_handler",

    "text!templates/registers.html",
], function($, ui, _, EditHandler, dataTemplate) {

    var registersView = function(getValues, setValue) {
        var divTopName = '#registers-top',
            divName = '#registers',
            dataRepresentation = 10,
            nextRepresentation = {10: 16, 16: 2, 2: 10},
            editHandler = new EditHandler(divName, _afterEdit);;

        $(divTopName + " .sort").click(_sort);
        $(divTopName + " .data-representation").click(_changeDataRepresentation);
        
        function _sort() {
            var lastChanged = [];
            
            editHandler.endEdit();
            $(divName + ' .last-changed').each( function (index) { 
                lastChanged.push($(this).attr('id')); 
            });
            render(true);
            lastChanged.forEach( function (id) {
                $(divName + ' #' + id).addClass('last-changed');
            });
        }

        function _changeDataRepresentation() {
            var oldRepresentation = dataRepresentation;
            dataRepresentation = nextRepresentation[dataRepresentation];;
            $(divName + ' .val').each(function() {
                if ($(this).prop("tagName") == 'INPUT') {
                    $(this).val(_toCurrentDataRepresentation(parseInt($(this).val(), oldRepresentation)));
                } else {
                    $(this).html(_toCurrentDataRepresentation(parseInt($(this).html(), oldRepresentation)));
                }
            });
        }

        function _toCurrentDataRepresentation(value) {
            if (dataRepresentation == 10) return value;
            value = value.toString(dataRepresentation).toUpperCase();
            while (dataRepresentation == 2 && value.length < 8) {
                value = '0' + value;
            }
            if (dataRepresentation == 16 && value.length < 2) {
                value = '0' + value;
            }
            return value;
        }

        function _afterEdit(el, index) {
            var name =  el.parent().attr('id').substr(1),
                value;
                
            setValue(name, parseInt(el.val(), dataRepresentation));
            getValues().forEach( function (r) {
                if (r.name == name) {
                    value = r.value;
                }
            });
            return value;
        }
        
        function render(sort) {
            editHandler.endEdit();
            if (sort == undefined || sort == false) {
                change(getValues().map( function (r) { 
                    return {index: r.name, value: r.value};
                }) , false);
            }
            else {
                $(divName).html(_.template(dataTemplate, {registers: getValues(), _toCurrentDataRepresentation: _toCurrentDataRepresentation}));
                $( "#registers-table" ).sortable({
                    start: function( event, ui ) {
                        editHandler.endEdit();
                    }
                });
                $( "#registers-table" ).disableSelection();
                $(divName + ' .val').dblclick(function() {
                    var index = $(divName + ' .val').index(this);
                    if (index >= 0) {
                        editHandler.startEdit(index, dataRepresentation);
                    }
                });
            }
        }

        function change(changes, mark) {
            var mark = mark == undefined ? true : mark;
            if (changes.length <= 0) return;
            $(divName + ' .val').removeClass('last-changed');
            
            changes.forEach(function(c) {
                if (mark == true) {
                    $(divName + ' #R' + c.index).addClass('last-changed');
                }
                $(divName + ' #R' + c.index).html(_toCurrentDataRepresentation(c.value));
            });
        }

        return {
            render: render,
            change: change,
        }
    };

    return registersView;
});
