define([
    "jquery",
    "underscore", 
    "bootstrap",

    "editor_manager",

    "text!templates/current_name.html",
], function ($, _, bootstrap, editorManager, currentNameTemplate) {

    var afterFunction = function() {},
        timer,
        timeout = 500;
    
    $("#show-save-as-modal").click(renderSaveAsModal);
    $("#save-current").click(saveCurrent);
    
    $("#save-as-modal #save").click(_save);
    $("#save-as-modal .cancel").click(_cancel);

    $('#save-program-name').keyup(function() {
        $('#save').prop('disabled', true);
        clearTimeout(timer);
        timer = setTimeout(_checkName, timeout);
    });

    function _checkName() {
        var name = $('#save-program-name').val(),
            pat = /^\w+$/,
            files = JSON.parse(localStorage.debbie_programs); 

        if (name != '...' && pat.test(name)) {
            $('#save').prop('disabled', false);
            if (name in files) {
                $('#save-warning').show();
            } 
            else {
                $('#save-warning').hide();
            }
        }
    }

    function _cancel() {
        afterFunction = function() {};
        $('#save-as-modal').modal('hide');
    }

    function saveCurrent() {
        var current =  localStorage.debbie_current,
            files,
            date = new Date().toLocaleString();

        if (current =='...') {
            renderSaveAsModal();
        } else {
            files = JSON.parse(localStorage.debbie_programs);
            files[current] = { name: current, text: editorManager.getText(), list: true, date: date };
            localStorage.debbie_programs = JSON.stringify(files);
            $("#current-program").html(_.template(currentNameTemplate, {changed: false, fileName: current}));                
        }
}

    function _save() {
        var current = $('#save-program-name').val();
        localStorage.debbie_current = current;
        saveCurrent();
        $('#save-as-modal').modal('hide');
        afterFunction();
        afterFunction = function() {};
    }

    function renderSaveAsModal(callback) {
        callback = callback !== undefined ? callback : function() {};
        afterFunction = callback;
        $('#save-as-modal').modal('show');
        $('#save-program-name').val('');
    }

    return {
        renderSaveAsModal: renderSaveAsModal,
        saveCurrent: saveCurrent,
    }
});
