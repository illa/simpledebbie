define([
    "jquery",
    "bootstrap",

    "save_as_view",
    "editor_manager",
], function ($, bootstrap, saveAsView, editorManager) {

    var afterFunction = function(){};

    $('#save-changes-modal .yes').click(function () {
        if (current == '...') {
            $('#save-changes-modal').modal('hide');
                saveAsView.renderSaveAsModal(afterFunction);
        } else {
            saveAsView.saveCurrent();
            $('#save-changes-modal').modal('hide');
            afterFunction();
        }
        afterFunction = function(){};
    });
    $('#save-changes-modal .no').click(function () {
        $('#save-changes-modal').modal('hide');
        afterFunction();
        afterFunction = function(){};
    });

    
    function _changesInCurrentFile() {
        var current = localStorage.debbie_current,
            files = JSON.parse(localStorage.debbie_programs);
        if (files[current].text != editorManager.getText()) {
            return true;
        }
        return false;
    }

    function check(callback) {
        afterFunction = callback;
        var current = localStorage.debbie_current;
                
        if (_changesInCurrentFile()) {
            $('#save-changes-modal').modal('show');
        } else {
            afterFunction();
        }
    }

    return {
        check: check,
    }
});
