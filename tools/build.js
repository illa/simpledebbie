({
    baseUrl: "../src/scripts/",
    mainConfigFile: '../src/scripts/main.js',
    paths: {
        requireLib: "../lib/require",
    },
    name: "main",
    include: ["requireLib"],
    out: "../min/main_min.js",
    removeCombined: true,
})
